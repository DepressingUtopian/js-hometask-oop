/**
 * 1. Допиши функцию-конструктор Leopard так, чтобы она унаследовала от Cat
 * все методы.
 * Сделай так, чтобы у Leopard метод sayMeow возвращал 'Roar!'
 * А метод getPet у Leopard должен возвращать ту строку, возвращает метод у Cat,
 * и кроме этого устанавливать леопарду, у которого был вызван метод, в поле
 * mood значение 'счастлив'
 */

function Cat() { };

Cat.prototype.sayMeow = () => {
  return 'Meow!   Meow!   Meow!';
};

Cat.prototype.getPet = () => {
  return 'Purrrrrr';
};

function Leopard() { };


Leopard.prototype = new Cat();

Leopard.prototype.sayMeow = function () {
  return 'Roar!';
};

Leopard.prototype.getPet = function () {
  this.mood = 'счастлив';
  return 'Purrrrrr';
};

/**
 * 2. Сделай так, чтобы при вызове функции changeMap,
 * метод map у всех массивов начал возвращать строку 'метод устал и не хочет работать!'
 *
 * Внимание! Никогда так не делай в реальных проектах!
 */

function changeMap() {
  Array.prototype.map = () => 'метод устал и не хочет работать!';
}

/**
 * 3. Перепиши функцию Tiger на аналогичный класс
 * (с ключевым словом class) с таким же названием.
 *
 * Не забудь удалить исходную функцию.
 */

class Tiger {
  constructor(name, type) {
    this.name = name;
    this.type = type;
  }

  countStrips() {
    this.strips = this.strips + 1 || 1;
  };
}

/**
 * 4. Функция Wolf наследует методы от функции Dog.
 * Перепиши обе функции и их родственные отношения на синтаксис class
 */

class Dog {
  sayWoof() {
    return 'Woof-Woof';
  };

  getFood() {
    this.fullness = (this.fullness || 0) + 25;
  };
}

class Wolf extends Dog {
  constructor(rank, isArcticWolf) {
    super();
    this.rank = rank;
    this.isArcticWolf = isArcticWolf;
  }

  hunt() {
    this.fullness = (this.fullness || 0) + 25;
  }
}

/**
 * 5. В природе существуют жабы, которые могут менять свой пол в течении жизни,
 * чтобы сохранять соотношение самцов и самок в популяции.
 *
 * Допиши классу Frog возможность при создании экземпляра класса указать пол:
 * поле sex, которое принимает значение 'male' или 'female';
 *
 * У всех лягушек должны быть общие, статические поля, подсчитывающие общее количество
 * самцов и самок в популяции. К полям должно быть можно обратиться так:
 *      Frog.maleCount и Frog.femaleCount
 *
 * Еще у лягушек должен быть общий, статический метод getGenderRatio, который считает
 * отношение количества самцов к количеству самок. Обратиться к нему должно быть можно так:
 *      Frog.getGenderRatio();
 *
 * У каждой лягушки должен быть метод updateGender, который проверяет
 * соотношение самцов и самок в популяции статическим методом из прошлого пункта.
 * Если соотношение ≤ 0.5, то лягушка должна стать самцом (если она не самец).
 * Если соотношения ≥ 1.5, то лягушка должна стать самкой (если она еще не самка).
 * Не забудь обновить количество в полях Frog.maleCount и Frog.femaleCount
 *
 */

class Frog {
  static maleCount = 0;
  static femaleCount = 0;

  constructor(sex) {
    if (sex === 'male') {
      Frog.maleCount += 1;
    }
    else {
      Frog.femaleCount += 1;
    }
    this.sex = sex;
  }

  static getGenderRatio() {
    return (this.femaleCount > 0) ? this.maleCount / this.femaleCount : 0;
  }

  updateGender() {
    const genderRatio = Frog.getGenderRatio();
    if (genderRatio <= 0.5 && this.sex !== 'male') {
      Frog.maleCount++;
      Frog.femaleCount--;
      this.sex = 'male';
    }

    if (genderRatio >= 1.5 && this.sex !== 'female') {
      Frog.femaleCount++;
      Frog.maleCount--;
      this.sex = 'female';
    }
  }
}

/**
 * 6. Допиши класс черепах (Turtle), который должен стать наследником класса рептилий (Reptile)
 *
 * У черепах должна быть возможность при создании указать, является ли она морской. Вот так:
 *      new Turtle(true) // морская черепаха
 *      new Turtle() // сухопутная черепаха
 * Если у морской черепахи вызвать метод run, то вместо этого
 * должен вызваться метод move (который унаследовался от Reptile) — черепаха не может бежать в воде.
 * У сухопутной черепахи при вызове метода run должен вызываться метод, унаследованный от рептилий.
 */

class Reptile {
  move() {
    this.speed = 1;
  }
  run() {
    this.speed = 2;
  }
}

class Turtle extends Reptile {
  isSea = false
  constructor(isSea) {
    super();
    if (isSea) {
      this.isSea = isSea;
    }
  }

  run() {
    if (this.isSea) {
      super.move();
    } else {
      super.run();
    }
  }
}



module.exports = {
  Leopard, Cat,
  changeMap,
  Tiger,
  Dog, Wolf,
  Frog,
  Reptile, Turtle,
};
