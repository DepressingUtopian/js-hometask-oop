# js-hometask-oop

## Ответьте на вопросы

### Откуда в объектах методы `toString` и `valueOf`, хотя вы их не добавляли?
> Ответ:
> Все объекты по умолчанию наследуются от базового класса Object. Разработчики спецификации языка определили методы `toString()` и `valueOf` в Object.prototype, поэтому они доступны >каждому классу.
### Чем отличаются `__proto__` и `prototype`? 
> Ответ:
> `prototype` - есть только у function и class
> `__proto__` - есть у всех объектов и таже у примитивных типов, если к ним обратиться через точку (.) 
> `__proto__` и `prototype` не всегда равны, например
```javascript
let num = new Number(1);
console.log(num.__proto__ === num.prototype); //false - у num нет prototype
console.log(num.__proto__ === Number.prototype); //true - у Number есть прототип

class Samurai {
    constructor(name) {
        this.name = name
    } 
    hello() {
        alert(this.name)
    }
}

console.log(Samurai.prototype === Samurai.__proto__); // false  -- у Samurai свой prototype, а __proto__ ссылается на Function.proto
```

### Что такое `super` в классах?
> Ответ:
> Ключевое слово super использовуется для вызова функций или конструктора родительского класса.
### Что такое статический метод класса, как его создать? 
> Ответ:
> Статический метод - метод доступный в каждом экземпляре класса, независимо от того создан ли объект. Статический метод можно объявить с помошью ключевого слова static. 

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `tasks.js`;
* Проверяте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
